package main

import (
	"math/rand"
	"strconv"
	"time"
)

const (
	letterBytes   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var rand_src = rand.NewSource(time.Now().UnixNano())

func id2pgid(id int64) string {
	return strconv.FormatInt(id, 36)
}

func pgid2id(s string) int64 {
	id, err := strconv.ParseInt(s, 36, 64)
	if err == nil {
		return id
	}
	return -1
}

func newEditKey(id int64) (string, error) {
	new_key := randString(32)
	_, err := db.Exec("INSERT INTO edit_key(entry_id, ctime, key) VALUES (?, ?, ?)", id, uint32(time.Now().Unix()), new_key)
	return new_key, err
}

func randString(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, rand_src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand_src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
