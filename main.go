package main

import (
	"database/sql"
	//"fmt"
	"encoding/json"
	"github.com/flosch/pongo2"
	"github.com/gorilla/context"
	"github.com/gorilla/sessions"
	"github.com/zenazn/goji"
	"github.com/zenazn/goji/bind"
	//"github.com/zenazn/goji/web"
	_ "github.com/mattn/go-sqlite3"
	//gomhttp "github.com/rakyll/gom/http"
	"io/ioutil"
	"log"
	"net/http"
	//"net/http/pprof"
	"os"
)

type webTemplates struct {
	index  *pongo2.Template
	intro  *pongo2.Template
	search *pongo2.Template
}

var (
	secret = "thisismysecret"
	ss     = sessions.NewCookieStore([]byte(secret))
	tpls   webTemplates
	db     *sql.DB
	cwd    string
)

func main() {
	configure()
	useMiddleware()
	bindRoutes()
	compileTemplates()
	openDatabase()

	goji.ServeListener(bind.Socket("0.0.0.0:8000"))

	closeDatabase()
}

type Config struct {
	Secret	string
}

func configure() {
	raw_json, err := ioutil.ReadFile("/etc/pgred_config.json")
	if err != nil {
		return
	}
	var cfg Config
	err = json.Unmarshal(raw_json, &cfg)
	if err == nil {
		secret = cfg.Secret
	} else {
		log.Println("Error parsing /etc/pgred_config.json: %v", err.Error())
	}
}

func useMiddleware() {
	goji.Use(context.ClearHandler)
}

func bindRoutes() {
	goji.Get("/p/:pg", view_pg)
	goji.Get("/", view_index)
	goji.Post("/pgops", view_pgops)
	goji.Get("/search", view_search)
	goji.Get("/api/get_edit_url", view_api_get_edit_url)
	bindDebugRoutes()

	// Bind the static file handler to the ${cwd}/static, as a catch-all on /static
	// In production, this should be handler by the top-level http server
	var err error
	cwd, err = os.Getwd()
	if err != nil {
		log.Fatal("Cannot getcwd")
	}
	log.Println("Serving static content from: " + cwd + "/static")
	goji.Handle("/static/*", http.FileServer(http.Dir(cwd)))
}

func bindDebugRoutes() {
	/*
		goji.Get("/debug/pprof/", http.HandlerFunc(pprof.Index))
		goji.Get("/debug/pprof/cmdline", http.HandlerFunc(pprof.Cmdline))
		goji.Get("/debug/pprof/profile", http.HandlerFunc(pprof.Profile))
		goji.Get("/debug/pprof/symbol", http.HandlerFunc(pprof.Symbol))
		goji.Get("/debug/pprofstats*", gomhttp.Stats())
	*/
}

func compileTemplates() {
	templates_dir := cwd + "/templates"
	log.Println("Compiling templates from: " + templates_dir)
	pongo2.DefaultLoader.SetBaseDir(templates_dir)
	tpls.index = pongo2.Must(pongo2.FromFile("index.html"))
	tpls.intro = pongo2.Must(pongo2.FromFile("intro.html"))
	tpls.search = pongo2.Must(pongo2.FromFile("search.html"))
}

func openDatabase() {
	dbName := cwd + "/db/db.sqlite3"
	var err error
	db, err = sql.Open("sqlite3", dbName)
	if err != nil {
		log.Fatal("Cannot open database: " + dbName)
	}
	_, err = db.Query("PRAGMA foreign_keys = ON;")
	if err != nil {
		log.Fatal("Cannot turn on foreign keys")
	}
}

func closeDatabase() {
	db.Close()
}
