CREATE TABLE entry (
    id          INTEGER PRIMARY KEY,
    ctime       INTEGER NOT NULL,
    mtime       INTEGER NOT NULL,
    title       TEXT NOT NULL,
    html        TEXT NOT NULL
);

CREATE INDEX entry_title ON entry(title);
