CREATE TABLE edit_key (
    id          INTEGER PRIMARY KEY,
    entry_id    INTEGER NOT NULL REFERENCES entry(id) ON UPDATE CASCADE ON DELETE CASCADE,
    ctime       INTEGER NOT NULL,
    key         TEXT
);

CREATE UNIQUE INDEX edit_key_key ON edit_key(key);
CREATE INDEX edit_key_entry ON edit_key(entry_id);
