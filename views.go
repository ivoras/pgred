package main

import (
	"encoding/json"
	"fmt"
	"github.com/flosch/pongo2"
	"github.com/microcosm-cc/bluemonday"
	"github.com/zenazn/goji/web"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var (
	session_name = "PGRED"
)

// Serves only the "/" page
func view_index(c web.C, w http.ResponseWriter, r *http.Request) {
	sess, err := ss.Get(r, session_name)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if val, ok := sess.Values["counter"]; ok {
		sess.Values["counter"] = val.(int) + 1
	} else {
		sess.Values["counter"] = 1
	}
	
	intro, _ := tpls.intro.Execute(pongo2.Context{})
	ctx := pongo2.Context{"hello": "world", "counter": sess.Values["counter"], "title": "Welcome!", "html": intro}

	sess.Save(r, w)
	tpls.index.ExecuteWriter(ctx, w)
}

// A page operation (create / edit, etc)
func view_pgops(c web.C, w http.ResponseWriter, r *http.Request) {
	sess, err := ss.Get(r, session_name)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if _, ok := sess.Values["counter"]; !ok {
		http.Error(w, "Spurious error", 500)
		return
	}

	op := r.PostFormValue("op")

	if op == "create" {
		newtitle := r.PostFormValue("newtitle")
		if newtitle == "" {
			newtitle = "Untitled"
		}
		now := uint32(time.Now().Unix())
		result, err := db.Exec("INSERT INTO entry(ctime, mtime, title, html) VALUES (?, ?, ?, ?)", now, now, newtitle, "")
		id, err := result.LastInsertId()
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		id_str := fmt.Sprintf("%d|", id)
		edit_key, err := newEditKey(id)
		if sess.Values["edit_auth"] != nil {
			sess.Values["edit_auth"] = sess.Values["edit_auth"].(string) + id_str
		} else {
			sess.Values["edit_auth"] = id_str
		}

		sess.Save(r, w)
		http.Redirect(w, r, fmt.Sprintf("/p/%s?key=%s", id2pgid(id), edit_key), 303)
		return
	} else if op == "edit" {
		title := r.PostFormValue("title")
		html := r.PostFormValue("html")
		now := uint32(time.Now().Unix())
		id, err := strconv.ParseInt(r.PostFormValue("id"), 10, 64)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		id_str := fmt.Sprintf("%d|", id)
		if !strings.Contains(sess.Values["edit_auth"].(string), id_str) {
			http.Error(w, "Who are you?", 403)
			return
		}
		html = bluemonday.UGCPolicy().Sanitize(html)
		_, err = db.Exec("UPDATE entry SET mtime=?, title=?, html=? WHERE id=?", now, title, html, id)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		sess.Save(r, w)
		http.Redirect(w, r, fmt.Sprintf("/p/%s", id2pgid(id)), 303)
	}

	sess.Save(r, w)
}

// View a page, from a /p/... url
func view_pg(c web.C, w http.ResponseWriter, r *http.Request) {
	pgid := c.URLParams["pg"]
	id := pgid2id(pgid)
	id_str := fmt.Sprintf("%d|", id)

	sess, err := ss.Get(r, session_name)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if val, ok := sess.Values["counter"]; ok {
		sess.Values["counter"] = val.(int) + 1
	} else {
		sess.Values["counter"] = 1
	}

	get := r.URL.Query()

	var (
		mtime    uint32
		title    string
		html     string
		editable = false
		editing  = false
	)
	err = db.QueryRow("SELECT mtime, title, html FROM entry WHERE id=?", id).Scan(&mtime, &title, &html)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if len(get["key"]) != 0 {
		var key_entry_id int64
		err = db.QueryRow("SELECT entry_id FROM edit_key WHERE key=?", get["key"][0]).Scan(&key_entry_id)
		if err == nil {
			if key_entry_id == id {
				editing = true
				if sess.Values["edit_auth"] == nil {
					sess.Values["edit_auth"] = ""
				}
				if !strings.Contains(sess.Values["edit_auth"].(string), id_str) {
					sess.Values["edit_auth"] = sess.Values["edit_auth"].(string) + id_str
				}
			}
		}
	}

	if sess.Values["edit_auth"] != nil {
		editable = strings.Contains(sess.Values["edit_auth"].(string), id_str)
		if editable && len(get["edit"]) > 0 {
			editing = true
		}
	}

	ctx := pongo2.Context{"counter": sess.Values["counter"], "title": title, "mtime": mtime, "html": html, "editable": editable, "editing": editing, "id": id, "pgid": pgid}

	sess.Save(r, w)
	tpls.index.ExecuteWriter(ctx, w)
}

type SearchResult struct {
	id    int64
	pgid  string
	mtime int
	title string
}

// Searches for page titles
func view_search(c web.C, w http.ResponseWriter, r *http.Request) {
	sess, err := ss.Get(r, session_name)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if _, ok := sess.Values["counter"]; !ok {
		http.Error(w, "Spurious error", 500)
		return
	}

	get := r.URL.Query()

	if len(get["q"]) == 0 {
		http.Redirect(w, r, "/", 303)
		return
	}
	q := get["q"][0]

	rows, err := db.Query("SELECT id, mtime, title FROM entry WHERE title LIKE ?", q + "%")
	defer rows.Close()

	results := []SearchResult{}
	for rows.Next() {
		var res SearchResult
		if err = rows.Scan(&res.id, &res.mtime, &res.title); err != nil {
			log.Println(err.Error())
			continue
		}
		res.pgid = id2pgid(res.id)
		results = append(results, res)
	}

	ctx := pongo2.Context{"counter": sess.Values["counter"], "results": results, "nresults": len(results), "q": q}

	sess.Save(r, w)
	tpls.search.ExecuteWriter(ctx, w)
}

type EditUrlResponse struct {
	Ok	bool
	Url	string
}

func view_api_get_edit_url(c web.C, w http.ResponseWriter, r *http.Request) {
	sess, err := ss.Get(r, session_name)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if _, ok := sess.Values["counter"]; !ok {
		http.Error(w, "Spurious error", 500)
		return
	}
	get := r.URL.Query()
	
	if len(get["id"]) == 0 {
		http.Error(w, "Spurious error", 500)
		return
	}
	id, err := strconv.ParseInt(get["id"][0], 10, 64)
	if err != nil {
		http.Error(w, "Spurious error", 500)
		return
	}
	id_str := fmt.Sprintf("%d|", id)
	
	var resp EditUrlResponse
	
	if sess.Values["edit_auth"] != nil {
		if strings.Contains(sess.Values["edit_auth"].(string), id_str) {
			resp.Ok = true
			edit_key, err := newEditKey(id)
			if err != nil {
				http.Error(w, "Spurious error", 500)
				return
			}
			resp.Url = fmt.Sprintf("/p/%s?key=%s", id2pgid(id), edit_key)
		}
	}
	
	json, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, "Spurious error", 500)
		return
	}
	
	w.Header().Set("Content-type", "application/json")
	w.Write(json)
}
